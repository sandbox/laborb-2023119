/**
 * @file
 * Utility functions to display settings summaries on vertical tabs.
 */

(function($) {

Drupal.behaviors.ucCloneCartItemSetSummary = {
  attach: function (context) {
    $('fieldset#edit-uc-clone-cart-item', context).drupalSetSummary(function(context) {
      var textRaw = '';
      if ($('#edit-uc-clone-cart-item-enabled', context).is(':checked')) {
        textRaw += 'Customers may clone cart item';
        if ($('#edit-uc-clone-cart-item-redirect', context).is(':checked')) {
          textRaw += ' and will be redirected to product form.';
        }
        else {
          textRaw += '.';
        }
      }
      else {
        textRaw += 'Customers are not allowed to clone cart item.';
      }

      return Drupal.t(textRaw);
    });
  }
};

})(jQuery);